---
title: #**Design Theory**
date: 2017 09 07
---


De week eindigde op donderdag 7 september met een theorieles over het ontwerpproces. Hierbij zaten we weer in de projectteams en was het de bedoeling om een probleem te bedenken die je vervolgens zelf hebt kunnen oplossen. 

We kozen voor het idee van Yana: 
Ze reisde door Azië en wilde met haar vriend (die in Nederland was) een bezoekje aan Japan brengen. Dit heeft ze zelf in een korte tijd kunnen regelen. 
Ik zorgde ervoor dat het idee verbeeld werd op papier en dit werd vervolgende door Lilian gepresenteerd. Na de presentatie ontvingen we feedback van de klas en van de lerares.

**Feedback**
 - Visueel erg sterk (simple, yet strong) 
 - Volgorde duidelijk 
 - Kort maar krachtig toegelicht 
 - Goede tekeningen
 
 


----------
Deveney Cyrus
0938387
CMD1B

> Written with [StackEdit](https://stackedit.io/).