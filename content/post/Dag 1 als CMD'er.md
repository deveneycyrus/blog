---
title: #**Dag 1 als CMD'er**
date: 2017-08-31
---

Op donderdag 31 augustus begon de eerste officiele lesdag van CMD.
Tijdens de introductie stond Project 1 centraal. 

De opdracht luidde het ontwerpen van een spel dat gebruik maakt van een online interactief element dat in de introductieweek van 2018 studenten in Rotterdam met elkaar en met de stad Rotterdam verbindt. Met als opdrachtgever Hogeschool Rotterdam. Met als resultaat: "Een speelbaar spel met offline en online component."

Na de introductie vond een hoorcollege over Game Design plaats. 
In dit hoorcollege werd de inhoud van een game besproken en een aantal belangrijke quotes over gaming. Tijdens dit hoorcollege leerde ik dat graphics niet het belangrijkste zijn in een game, Gameplay comes first!
Ook kwamen er 10 tips voor prototyping aanbod, dit waren handige tips om aan de slag te kunnen met het eerste project.


----------
Deveney Cyrus
0938387
CMD1B

> Written with [StackEdit](https://stackedit.io/).